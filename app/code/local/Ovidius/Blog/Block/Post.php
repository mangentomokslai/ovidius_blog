<?php
class Ovidius_Blog_Block_Post extends Ovidius_Blog_Block_Blog
{
    public function getPost()
    {
        if(Mage::registry('post'))
            return Mage::registry('post');
        else {
            $postId = $this->getRequest()->getParam('id');
            $post = Mage::getModel('blog/post')->load($postId);
            Mage::register('post', $post);
            return $post;
        }
    }

    public function isAuthorLoggedIn()
    {
        if (!Mage::getSingleton('customer/session')->isLoggedIn())
            return false;

        $customerId = Mage::getSingleton('customer/session')->getId();
        $authorId = $this->getPost()->getAuthorId();

        if($customerId == $authorId)
            return true;
        else
            return false;
    }
}
