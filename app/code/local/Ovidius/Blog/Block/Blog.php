<?php
class Ovidius_Blog_Block_Blog extends Mage_Core_Block_Template
{
    public function getAllPosts()
    {
        return Mage::getResourceModel('blog/post_collection')
            ->addFieldToFilter('status', array( 'eq' => 'published'));
    }

    public function getCurrentAuthorPosts()
    {
        $authorId = Mage::getSingleton('customer/session')->getId();
        if ($authorId)
            return Mage::getResourceModel('blog/post_collection')
                ->addFieldToFilter('author_id', array( 'eq' => $authorId));
            else
                return new Varien_Collection;
    }
}
