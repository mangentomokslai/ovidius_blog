<?php
class Ovidius_Blog_Block_Adminhtml_Blog_Post extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        parent::__construct();
        $this->_blockGroup = 'blog';
        $this->_controller = 'adminhtml_blog_post';
        $this->_headerText = Mage::helper('blog')->__('Blog posts');
        $this->_addButtonLabel = Mage::helper('blog')->__('Add New Blog Post');
    }

}
