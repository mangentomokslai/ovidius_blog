<?php
class Ovidius_Blog_Block_Adminhtml_Blog_Post_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('blog_post_tabs');
        $this->setDestElementId('edit_form');
    }

    protected function _beforeToHtml()
    {
        $this->addTab('general_section', array(
            'label' => Mage::helper('blog')->__('General Information'),
            'title' => Mage::helper('blog')->__('General Information'),
            'content' => $this->getLayout()->createBlock('blog/adminhtml_blog_post_edit_tab_general')->toHtml(),
        ));

        return parent::_beforeToHtml();
    }
}
