<?php
class Ovidius_Blog_Block_Adminhtml_Blog_Post_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
        $this->_objectId = 'entity_id';
        $this->_controller = 'adminhtml_blog_post';
        $this->_blockGroup = 'blog';

        $post = Mage::registry('current_post');

        $this->_updateButton('delete', 'label', Mage::helper('blog')->__('Delete'));

        $this->_addButton('saveandcontinue', array(
            'label' => Mage::helper('blog')->__('Save And Continue Edit'),
            'onclick' => 'saveAndContinueEdit()',
            'class' => 'save',
        ), -100);

        $this->_formScripts[] = "
            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action + 'back/edit/');
            }
        ";
    }

    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
        }
    }

    public function getPost()
    {
        if (Mage::registry('current_post') && Mage::registry('current_post')->getId()) {
            return Mage::registry('current_post');
        }
    }

    public function getHeaderText()
    {
        if ($post = $this->getPost()) {
            return Mage::helper('blog')->__("Edit Blog Post '%s'", $this->htmlEscape($post->getTitle()));
        } else {
            return Mage::helper('blog')->__('Create New Post');
        }
    }

    public function _toHtml()
    {
        $html = parent::_toHtml();

        return $html;
    }
}
