<?php
class Ovidius_Blog_Block_Adminhtml_Blog_Post_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('grid');
        $this->setUseAjax(true);
        $this->setDefaultSort('entity_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('blog/post')->getCollection()
            ->join(array('c' => 'customer/entity'), 'main_table.author_id = c.entity_id', array(
                'email'       => 'email',
            ));

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $helper = Mage::helper('blog');

        $this->addColumn('entity_id', array(
            'header' => $helper->__('Post #'),
            'index'  => 'entity_id'
            )
        );

        $this->addColumn('title', array(
            'header' => $helper->__('Title'),
            'index' => 'title',
            'filter_index' => 'main_table.title',
            )
        );

        $this->addColumn('email', array(
            'header' => $helper->__('Author'),
            'index' => 'email',
            'filter_index' => 'c.email',
            )
        );

        $statuses = array(
            'draft' => Mage::helper('blog')->__('Draft'),
            'published' => Mage::helper('blog')->__('Published'),
        );

        $this->addColumn('status', array(
            'header'    =>  Mage::helper('blog')->__('Status'),
            'width'     =>  '100',
            'index'     =>  'status',
            'type'      =>  'options',
            'options'   =>  $statuses,
        ));

        $this->addExportType('*/*/exportCsv', Mage::helper('customer')->__('CSV'));
        $this->addExportType('*/*/exportXml', Mage::helper('customer')->__('Excel XML'));

        return parent::_prepareColumns();
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('entity_id');
        $this->getMassactionBlock()->setFormFieldName('post');

        $this->getMassactionBlock()->addItem('publish', array(
             'label'    => Mage::helper('blog')->__('Publish'),
             'url'      => $this->getUrl('*/*/massPublish'),
        ));

        $this->getMassactionBlock()->addItem('delete', array(
             'label'    => Mage::helper('blog')->__('Delete'),
             'url'      => $this->getUrl('*/*/massDelete'),
             'confirm'  => Mage::helper('blog')->__('Are you sure?')
        ));

        return $this;
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current' => true));
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

}
