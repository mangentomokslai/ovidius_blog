<?php
class Ovidius_Blog_Block_Adminhtml_Blog_Post_Edit_Tab_General extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $post = Mage::registry('current_post');

        $fieldset = $form->addFieldset('edit_fieldset', array('legend' => Mage::helper('blog')->__('General Information')));
        if ($post->getId()) {
            $fieldset->addField('entity_id', 'hidden', array(
                'name' => 'entity_id',
                'value' => $post->getId(),
            ));
        }

        $fieldset->addField('title', 'text', array(
            'label' => Mage::helper('blog')->__('Title'),
            'required' => true,
            'name' => 'title',
            'value' => $post->getTitle(),
        ));

        $fieldset->addField('body', 'textarea', array(
            'label' => Mage::helper('blog')->__('Body'),
            'required' => true,
            'name' => 'body',
            'value' => $post->getBody(),
        ));

        $fieldset->addField('author', 'select', array(
            'label' => Mage::helper('blog')->__('Author'),
            'required' => true,
            'name' => 'author_id',
            'value' => $post->getAuthorId(),
            'values' => Mage::getModel('customer/customer')->getCollection()->toOptionArray(),
        ));

        $fieldset->addField('status', 'select', array(
            'label' => Mage::helper('blog')->__('Status'),
            'required' => true,
            'name' => 'status',
            'value' => $post->getStatus(),
            'values' => array(
                'draft' => Mage::helper('blog')->__('Draft'),
                'published' => Mage::helper('blog')->__('Published'),
                )
            )
        );


        return parent::_prepareForm();
    }

}
