<?php
$installer = $this;
$installer->startSetup();

$installer->run('UPDATE ' . $installer->getTable('blog/post') . ' SET status = \'published\'');

$installer->endSetup();
