<?php
$installer = $this;
$installer->startSetup();

// Define data
$posts = array(
    array(
        'title' => 'Lorem ipsum',
        'body' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean augue purus, imperdiet dignissim mi at, tincidunt viverra magna. Vestibulum at tempor purus. Donec at ipsum ut erat lacinia congue. Maecenas non viverra arcu. Phasellus laoreet tempus bibendum. Morbi id laoreet eros, auctor luctus justo. Suspendisse potenti. Curabitur nunc nisi, suscipit scelerisque ultrices sed, molestie et nulla. Integer eget ipsum ac erat condimentum egestas vel vel augue. Donec in imperdiet dui, quis dignissim diam. Proin in enim eget felis pellentesque tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Pellentesque justo quam, euismod id dolor ut, egestas pharetra justo. Vivamus luctus lacus sit amet sapien sodales dictum. Vivamus rutrum sapien ultrices sapien vulputate, sagittis malesuada ante scelerisque. Etiam leo neque, ultricies sit amet condimentum sed, sollicitudin ut orci.',
        'author_id' => 1
    ),
    array(
        'title' => 'Donec porta consectetur',
        'body' => 'Donec porta consectetur purus at congue. Vestibulum at luctus mauris. Ut mauris neque, luctus ac ullamcorper eu, tempus id nibh. Praesent sed lectus gravida, suscipit arcu a, cursus est. Praesent congue massa sollicitudin odio tristique imperdiet. In tempor enim id nibh lacinia, vel faucibus odio suscipit. Fusce rhoncus vitae diam at fermentum. Sed sit amet nisl eget libero hendrerit aliquet quis eget turpis. Fusce ac dictum ipsum, vel dictum purus. Integer sed nisi id erat luctus bibendum eget a tortor.',
        'author_id' => 1
    ),
    array(
        'title' => 'Vestibulum imperdiet est id mollis finibus',
        'body' => 'Vestibulum imperdiet est id mollis finibus. In in ullamcorper sem. Maecenas urna arcu, accumsan sit amet neque sit amet, lobortis finibus tellus. Quisque hendrerit feugiat risus, scelerisque faucibus nisl hendrerit vel. Sed placerat luctus arcu, nec consequat odio pharetra eget. Duis volutpat elit sed vulputate laoreet. Mauris vehicula diam non mi gravida bibendum. In consectetur, velit id ornare luctus, est nibh venenatis tortor, id elementum sem urna sit amet magna. Quisque euismod bibendum consectetur. Cras commodo faucibus tellus, vitae egestas metus pharetra nec.',
        'author_id' => 1
    ),
    array(
        'title' => 'Aliquam eget odio vehicula',
        'body' => 'Aenean elementum nec sapien vel accumsan. Aenean ut massa id lacus tincidunt luctus. Maecenas nec enim cursus, lobortis risus a, rhoncus tellus. Sed auctor ultrices diam. Nam eleifend in diam a facilisis. Sed nulla urna, mollis non sapien ultricies, cursus pellentesque erat. Etiam porta quam a tortor volutpat vestibulum. Aliquam eget odio vehicula, viverra eros quis, tempus neque. Morbi vestibulum, mi et lobortis eleifend, ligula odio faucibus sem, id blandit ex sapien sit amet augue. Nullam ullamcorper magna ac quam bibendum venenatis. Vivamus ipsum nunc, imperdiet vel tortor ac, euismod pharetra mi. Curabitur et efficitur orci, vitae pharetra nisi. Phasellus auctor dapibus dignissim. Quisque volutpat quis nisi iaculis dapibus. Maecenas mollis dignissim ultrices. Vestibulum facilisis nisl a libero rhoncus, et pellentesque nibh venenatis.',
        'author_id' => 1
    )
);

// Insert data into table
foreach ($posts as $post) {
    Mage::getModel('blog/post')
        ->setData($post)
        ->save();
}

$installer->endSetup();
