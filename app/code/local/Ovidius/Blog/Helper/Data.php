<?php
class Ovidius_Blog_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function getPostLink($id)
    {
        return Mage::getBaseUrl() . "blog/post/view/id/{$id}";
    }
}
