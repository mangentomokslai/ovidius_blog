<?php
class Ovidius_Blog_PostController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        $this->loadLayout()->renderLayout();
    }

    public function viewAction()
    {
        $this->loadLayout()->renderLayout();
    }

    public function createAction()
    {
        if (Mage::getSingleton('customer/session')->isLoggedIn())
            $this->loadLayout()->renderLayout();
        else
            $this->_redirect('customer/account/login');
    }

    public function storeAction()
    {
        $params = $this->getRequest()->getParams();
        $authorId = Mage::getSingleton('customer/session')->getId();

        if (!isset($authorId))
            $this->_redirect('customer/account/login');

        try {
            if (!isset($params['name']) || $params['name'] == '' || !isset($params['body']) || $params['body'] == '') {
                if (!isset($params['name']) || $params['name'] == '')
                    throw new Exception('Post title not set');
                elseif (!isset($params['body']) || $params['body'] == '')
                    throw new Exception('Post body not set');
            }

            $post = Mage::getModel('blog/post');
            $post->setTitle($params['name']);
            $post->setBody($params['body']);
            $post->setAuthorId($authorId);
            $post->setStatus($params['status']);
            $post->save();

            if ($postId = $post->getId())
                $this->_redirect("blog/post/view/id/{$postId}");
            else {
                throw new Exception('Failed to create new blog post');
                $this->loadLayout()->renderLayout();
            }
        } catch(Exception $e) {
            Mage::register('blog_msg', $e->getMessage());
        }

        $this->loadLayout()->renderLayout();
    }

    public function editAction()
    {
        if (Mage::getSingleton('customer/session')->isLoggedIn()) {
            $postId = $this->getRequest()->getParam('id');
            $authorId = Mage::getModel('blog/post')
                ->load($postId)
                ->getAuthorId();

            if (Mage::getSingleton('customer/session')->getId() == $authorId)
                $this->loadLayout()->renderLayout();
            else
                $this->_redirect('');
        } else
            $this->_redirect('customer/account/login');
    }

    public function updateAction()
    {
        $postId = $this->getRequest()->getParam('id');
        $params = $this->getRequest()->getParams();

        if (Mage::getSingleton('customer/session')->isLoggedIn()) {
            $authorId = Mage::getModel('blog/post')
                ->load($postId)
                ->getAuthorId();

            if (Mage::getSingleton('customer/session')->getId() == $authorId) {
                try {
                    if ($params['name'] == '' || $params['body'] == '') {
                        if ($params['name'] == '')
                            throw new Exception('Post title not set');
                        elseif ($params['body'] == '')
                            throw new Exception('Post body not set');
                    }
                    if ($postId != '') {
                        $post = Mage::getModel('blog/post')->load($postId);
                        $post->setTitle($params['name']);
                        $post->setBody($params['body']);
                        $post->setStatus($params['status']);
                        $post->save();
                        $this->_redirect("blog/post/view/id/{$postId}");
                    }
                } catch (Exception $e) {
                    Mage::register('blog_msg', $e->getMessage());
                    $this->loadLayout()->renderLayout();
                }
            }
            else
                $this->_redirect('');

        } else
            $this->_redirect('customer/account/login');
    }

    public function destroyAction()
    {
        $postId = $this->getRequest()->getParam('id');
        if (Mage::getSingleton('customer/session')->isLoggedIn()) {
            $authorId = Mage::getModel('blog/post')
                ->load($postId)
                ->getAuthorId();

            if (Mage::getSingleton('customer/session')->getId() == $authorId) {
                try {

                    if ($postId != '') {
                        $post = Mage::getModel('blog/post')->load($postId);
                        $post->delete();
                        $this->_redirect('blog');
                    } else {
                        throw new Exception('Invalid post id');
                    }
                } catch(Exception $e) {
                    if ($postId) {
                        Mage::register('blog_msg', $e->getMessage());
                        $this->loadLayout()->renderLayout();
                    } else {
                        $this->_redirect('blog');
                    }
                }
            } else
                $this->_redirect('');
        } else
            $this->_redirect('customer/account/login');

    }

}
