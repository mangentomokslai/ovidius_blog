<?php
class Ovidius_Blog_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        $this->loadLayout()->renderLayout();
    }

    public function myPostsAction()
    {
        if (Mage::getSingleton('customer/session')->isLoggedIn())
            $this->loadLayout()->renderLayout();
        else
            $this->_redirect('customer/account/login');
    }

}
