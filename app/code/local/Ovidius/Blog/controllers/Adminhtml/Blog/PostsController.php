<?php
class Ovidius_Blog_Adminhtml_Blog_PostsController extends Mage_Adminhtml_Controller_Action
{
    protected function _initAction()
    {
        $this->loadLayout()->_setActiveMenu('blog/posts');

        return $this;
    }

    private function _initPost()
    {
        $post = Mage::getModel('blog/post');
        if ($this->getRequest()->getParam('id')) {
            $post->load($this->getRequest()->getParam('id'));
        }

        Mage::register('current_post', $post);
        return $post;
    }

    public function indexAction()
    {
        $this->_title($this->__('Blog posts'));
        if ($this->getRequest()->getQuery('ajax')) {
            $this->_forward('grid');
            return;
        }

        $this->_initAction();
        $this->loadLayout();
        $this->renderLayout();
        // $this->_addContent($this->getLayout()
        //     ->createBlock('blog/adminhtml_blog_post'));
        // $this->renderLayout();
    }

    public function gridAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }


    public function editAction()
    {
        $post = $this->_initPost();

        if ($post->getId()) {
            $this->_title($this->__("Edit blog post '%s'", $post->getName()));
            $this->_initAction();
            $this->loadLayout();
            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Blog post does not exist.'));
            $this->_redirect('*/*/');
        }
    }

    public function newAction()
     {
         $this->_title($this->__('New blog post'));
         $this->_initPost();
         $this->_initAction();
         $this->loadLayout();
         $this->renderLayout();
     }

    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost()) {
            $post = $this->_initPost();
            $post->addData($data);

            try {
                $post->save();

                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Post was successfully saved'));
                Mage::getSingleton('adminhtml/session')->setFormData(false);

                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $post->getId(), 'store' => $post->getStoreId()));

                    return;
                }
                $this->_redirect('*/*/');

                return;
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));

                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Unable to find post to save'));
        $this->_redirect('*/*/');
    }

    public function deleteAction()
    {
        if ($this->getRequest()->getParam('id') > 0) {
            try {
                $earningRule = Mage::getModel('blog/post');

                $earningRule->setId($this->getRequest()
                    ->getParam('id'))
                    ->delete();

                Mage::getSingleton('adminhtml/session')->addSuccess(
                        Mage::helper('adminhtml')->__('Blog post was successfully deleted'));
                $this->_redirect('*/*/');
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()
                    ->getParam('id'), ));
            }
        }
        $this->_redirect('*/*/');
    }

    public function massDeleteAction()
    {
        $postsIds = $this->getRequest()->getParam('post');
        if(!is_array($postsIds)) {
             Mage::getSingleton('adminhtml/session')->addError(Mage::helper('blog')->__('Please select blog post(s).'));
        } else {
            try {
                $post = Mage::getModel('blog/post');
                foreach ($postsIds as $postId) {
                    $post->load($postId)->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__('Total of %d blog posts(s) were deleted.', count($postsIds))
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }

        $this->_redirect('*/*/index');
    }

    public function massPublishAction()
    {
        $postsIds = $this->getRequest()->getParam('post');
        if(!is_array($postsIds)) {
             Mage::getSingleton('adminhtml/session')->addError(Mage::helper('blog')->__('Please select blog post(s).'));
        } else {
            try {
                $post = Mage::getModel('blog/post');
                foreach ($postsIds as $postId) {
                    $post->load($postId)->setStatus('published')->save();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__('Total of %d blog posts(s) were published.', count($postsIds))
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }

        $this->_redirect('*/*/index');
    }

    public function exportCsvAction()
    {
        $fileName   = 'blog_posts.csv';
        $content    = $this->getLayout()->createBlock('blog/adminhtml_blog_post_grid')
            ->getCsvFile();

        $this->_prepareDownloadResponse($fileName, $content);
    }

    public function exportXmlAction()
    {
        $fileName   = 'blog_posts.xml';
        $content    = $this->getLayout()->createBlock('blog/adminhtml_blog_post_grid')
            ->getExcelFile();

        $this->_prepareDownloadResponse($fileName, $content);
    }

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('blog/posts');
    }
}
