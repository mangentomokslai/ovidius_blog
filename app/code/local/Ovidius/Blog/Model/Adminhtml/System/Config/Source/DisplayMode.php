<?php
class Ovidius_Blog_Model_Adminhtml_System_Config_Source_DisplayMode
{
    public function toOptionArray()
    {
        return array(
            array('value' => 'p', 'label'=>Mage::helper('blog')->__('Post title')),
            array('value' => 'pe', 'label'=>Mage::helper('blog')->__('Post title + Excerpt')),
            array('value' => 'pea', 'label'=>Mage::helper('blog')->__('Post title + Excerpt + Author'))
        );
    }
}
