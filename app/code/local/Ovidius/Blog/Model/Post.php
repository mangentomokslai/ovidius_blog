<?php
class Ovidius_Blog_Model_Post extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        // Init resource model
        // It's passed to _setResourceModel which will also setup
        // collection model by appending _collection to 
        $this->_init('blog/post');
    }

    public function getAuthor()
    {
        return Mage::getModel('customer/customer')
            ->load($this->author_id);
    }

    public function getAuthorName()
    {
        return $this->getAuthor()->getName();
    }

}
