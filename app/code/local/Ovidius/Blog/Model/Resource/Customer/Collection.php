<?php
class Ovidius_Blog_Model_Resource_Customer_Collection extends Mage_Customer_Model_Resource_Customer_Collection
{
    public function toOptionArray()
    {
        return $this->_toOptionArray('entity_id', 'email');
    }
}
