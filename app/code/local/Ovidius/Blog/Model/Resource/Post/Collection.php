<?php
class Ovidius_Blog_Model_Resource_Post_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    public function _construct()
    {
        // Method _init sets model on which collection is based. 
        $this->_init('blog/post');
    }
}
