<?php
class Ovidius_Blog_Model_Resource_Post extends Mage_Core_Model_Resource_Db_Abstract {

    protected function _construct()
    {
        // initialize resource model:
        //   $mailTable - entity - it maps to config xlm node - config/global/models/blog_resource/entities/post,
        //   $idFieldName - primary key in db table
        // _init($mainTable, $idFieldName)
        $this->_init('blog/post', 'entity_id');
    }
}
