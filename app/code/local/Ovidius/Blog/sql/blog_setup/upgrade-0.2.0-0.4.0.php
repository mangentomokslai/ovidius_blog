<?php

$installer = $this;
$installer->startSetup();

$con = $installer->getConnection();
$con->addColumn($installer->getTable('blog/post'), 'status', array(
    'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
    'nullable'  => false,
    'length'    => 32,
    'after'     => 'author_id',
    'comment'   => 'Post status'
    ));

$installer->endSetup();
