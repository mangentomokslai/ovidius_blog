<?php

$installer = $this;

$installer->startSetup();

// Delete tables from previous version
$con = $installer->getConnection();

if ($installer->tableExists('blog/post'))
    $installer->run('DROP TABLE ' . $installer->getTable('blog/post'));

// Create blog_posts table
// Associate post author with customer entity
$table = $installer->getConnection()
    ->newTable($installer->getTable('blog/post'))
    ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        ), 'Id')
    ->addColumn('title', Varien_Db_Ddl_Table::TYPE_VARCHAR, null, array(
        'nullable'  => false,
        ), 'Title')
    ->addColumn('body', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable'  => false,
    ), 'Body')
    ->addColumn('author_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
    ), 'Author Id')
    ->addIndex($installer->getIdxName('blog/post', array('author_id')),
        array('author_id'))
    /* getFkName($priTableName, $priColumnName, $refTableName, $refColumnName) */
    ->addForeignKey(
        $installer->getFkName('blog/post', 'entity_id', 'customer/entity', 'entity_id'), // foreign key identifier
        'author_id',    //  foreign key name
        $installer->getTable('customer/entity'),  // ref. table
        'entity_id',      // ref. table primary key
        Varien_Db_Ddl_Table::ACTION_CASCADE,
        Varien_Db_Ddl_Table::ACTION_CASCADE);
$installer->getConnection()->createTable($table);

$installer->endSetup();
