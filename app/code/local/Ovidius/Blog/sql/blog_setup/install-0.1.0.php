<?php
$installer = $this;
$installer->startSetup();
//http://alanstorm.com/magento_setup_resources/
// create blog_posts table
$table = $installer->getConnection()
    ->newTable($installer->getTable('blog/post'))
    ->addColumn('post_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        ), 'Id')
    ->addColumn('title', Varien_Db_Ddl_Table::TYPE_VARCHAR, null, array(
        'nullable'  => false,
        ), 'Title')
    ->addColumn('body', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable'  => false,
    ), 'Body')
    ->addColumn('author', Varien_Db_Ddl_Table::TYPE_VARCHAR, null, array(
        'nullable'  => false,
    ), 'Author');
$installer->getConnection()->createTable($table);

$installer->endSetup();
