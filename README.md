# README

Blog module with CRUD functionality

#Routes:
    /blog           blog index page
    /blog/post/*    post routes (view, create, edit, destroy etc.)

# UPDATES
0.2.0

    + post association with customers

        + post can be modified (edited, deleted) per customer

    + improved validation

0.3.0

    + Admin CRUD (gridview, edit, new, delete)

0.3.1

    + System/Config options: blog name, overview display mode

0.3.2

    + Massaction and export (CSV/XML) features for posts

0.4.0

    + Post status attribute (published, draft)

    + Publish massaction

    + "My posts" page
